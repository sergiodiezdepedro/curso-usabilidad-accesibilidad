# Introducción a las Pruebas de Usabilidad, UX y Accesibilidad

¿Qué es Usabilidad?

> La capacidad de un software de ser comprendido, aprendido, usado y ser atractivo para el usuario en condiciones específicas de uso.
>
> ISO/IEC 9126.

> Usabilidad es la eficacia, eficiencia y satisfacción con la que un producto permite alcanzar objetivos específicos a usuarios específicos en un contexto de uso específico.
>
> ISO/IEC 9241

> Atributo de calidad que mide lo fáciles que son de usar las interfaces Web.
>
> Jakob Nielsen.

**Usabilidad = Facilidad de uso**.

La usabilidad siempre se relaciona con el contexto de uso y puede considerarse en diferentes situaciones.

La evaluación de usabilidad aborda la interacción directa entre los usuarios y el producto de software.

## Evaluación de usabilidad

Sus objetivos son:

- Evaluar si se han cumplido los requisitos de usabilidad.
- Descubrir problemas de usabilidad para que puedan corregirse.
- Medir la usabilidad de un producto de software.

La evaluación de usabilidad aborda lo siguiente:

**Efectividad**:

La medida en que se alcanzan las metas correctas y completas.

Responde a la pregunta: "¿El producto de software hace lo que quiero?"

**Eficiencia**:

Recursos gastados para lograr objetivos específicos.

Responde a la pregunta: "¿El producto de software resuelve mis tareas rápidamente?"

**Satisfacción**:

Libre de molestias y actitudes positivas hacia el uso del producto de software.

Responde la pregunta: "¿Me siento cómodo mientras uso el producto de software?"

Una **evaluación de usabilidad cualitativa** permite la identificación y el análisis de los problemas de usabilidad, enfocándose en comprender las necesidades, objetivos y razones del usuario para el comportamiento observado del usuario.

Una **evaluación cuantitativa de usabilidad** se enfoca en obtener mediciones para la efectividad, eficiencia o satisfacción de un producto de software.

## Requisitos de usabilidad

Los requisitos de usabilidad pueden tener una variedad de fuentes:

- Pueden expresarse explícitamente, como en la documentación de requisitos, o en una historia de usuario.
- Pueden ser expectativas implícitas del usuario (por ejemplo, un usuario podría esperar implícitamente que una aplicación proporciona teclas de acceso directo para acciones particulares del usuario).
- También pueden incluirse en estándares adoptados o requeridos.

Ejemplos de requisitos de usabilidad (en este caso descritos como historias de usuarios) son:

"Como usuario frecuente del portal de reservas de la aerolínea, una descripción general de mis vuelos reservados actualmente se mostrará automáticamente después de iniciar sesión. Esto me permitirá obtener una visión general y rápida de mis reservas de vuelos y rápidamente hago cualquier actualización".

"Como asistente de la mesa de ayuda, debo poder ingresar y registrar los detalles de una solicitud del cliente en la base de datos de Customer Relations en no más de dos simples pasos. Esto me permitirá centrarme en la solicitud del cliente y proporcionarles un soporte óptimo".

Un producto de software puede funcionar exactamente según las especificaciones y aún tener serios problemas de usabilidad.

| Una aplicación móvil de reservas tiene un enlace inactivo.   | Este es un defecto que resulta en un problema de usabilidad. |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Una aplicación móvil de reservas permite a los usuarios cancelar una reserva, pero los usuarios perciben el procedimiento de cancelación como excesivamente complicado. | Este es un problema de usabilidad que afecta la eficiencia de la aplicación móvil. |
| Una aplicación móvil de reservas cumple con las especificaciones y funciona de manera efectiva y eficiente, pero los usuarios piensan que se ve poco profesional. | Este es un problema de usabilidad que afecta a la satisfacción del usuario al usar la aplicación móvil. |

![Human Centered Design](img/human-centered-design.png)

Resumiendo...

**Analizar**: hablar con la gente y descubrir *¿cuál es el problema?*

**Diseño**: prototipo de lo que asume que es una solución.

**Evaluar**: ver a las personas usar el prototipo y aprender de sus experiencias.

**Iterar**: repetir hasta que se cumplan los requisitos de usabilidad.

## Evaluación de Usabilidad Ágil

RITE Method, for Rapid Iterative Testing and Evaluation: https://en.wikipedia.org/wiki/RITE_Method

## Riesgos de usabilidad

Los riesgos se pueden identificar mediante una o más de las siguientes técnicas:

- Entrevistas.
- Talleres de riesgo.
- Lluvia de ideas.
- Recurrir a experiencias pasadas.

Riesgos típicos del **producto** relacionados con la usabilidad:

- Los usuarios no comprarán ni usarán el producto de software porque carece de efectividad, eficiencia o satisfacción.
- Los usuarios compran el producto de software, pero lo devuelven y exigen la devolución de su dinero porque no pueden hacer que funcione.
- El producto de software funciona según lo especificado, pero los usuarios no pueden descubrir cómo usarlo.
- Los usuarios compran el producto de software pero repetidamente necesitan llamar a soporte porque no entienden cómo usarlo.

Riesgos del **proyecto** relacionados con la usabilidad:

- Riesgos organizacionales.
- Falta de especialistas calificados en usabilidad, experiencia de usuario o accesibilidad.
- Conocimiento insuficiente de los principios básicos de usabilidad de los responsables del diseño y desarrollo del producto y sus procesos y artefactos asociados.
- Falta de conocimiento, criterios y proceso para seleccionar especialistas calificados en usabilidad.
- Baja madurez de usabilidad en la organización.

## Métodos de evaluación de usabilidad

- Heurísticas de usabilidad y Checklist.
- Card Sorting.
- Pruebas/encuestas con usuarios.

### Heurísticas de usabilidad

10 reglas de Jakob Nielsen:

1. Visibilidad del estado del sistema.
2. Relación entre el sistema y el mundo real.
3. Control y libertad por parte del usuario.
4. Consistencia y estándares.
5. Prevención de errores.
6. Reconocimiento antes que recuerdo.
7. Flexbilidad y eficiencia en el uso.
8. Estética y diseño minimalistas.
9. Ayudar a los usuarios a reconocer, diagnosticar y recuperarse de los errores.
10. Ayuda y documentación.

### Card Sorting

Es un método utilizado para ayudar a diseñar o evaluar la arquitectura de información de un sitio.

En una sesión de clasificación de tarjetas los participantes organizan los temas en categorías que tienen sentido para ellos y también pueden colaborar a etiquetar en grupos. 

Para realizar una clasificación de tarjetas se pueden usar tarjetas reales, trozos de papel, o una de las varias herramientas de software de card sorting online.

Beneficios del Card Sorting:

El Card Sorting nos ayudará a comprender las expectativas de nuestros usuarios y la comprensión de los temas.

A menudo es más útil, una vez que se hayan realizado algunas tareas previas para conocer a nuestros usuarios y comprender su contexto.

La información de un grupo de usuarios puede ayudarnos a:

- Construir la estructura para tu sitio web.
- Decidir qué poner en la página de inicio.
- Categorizar etiquetas y navegación.

Existen tres tipos de card sorting:

- **Card sorting abierto**. Es el más común. Los usuarios son libres de asignar nombres a los grupos que han creado y a organizar sus tarjetas. Tiene como fin descubrir y definir el tipo de clasificación de categoría más correcto de utilizar.
- **Card sorting cerrado**. Este tipo es más restringido. Se le pide a los participantes que clasifiquen contenido en categorías que ya se fijaron previamente. Es el más rápido de aplicar, dado que existe un camino ya delimitado por los moderadores.
- **Card sorting mixto**. Es una mezcla de los manteriores. Lo que se hace habitualmente es iniciar el proceso con una tarjeta abierta. Con ella, los usuarios deben ordenar los contenidos que el moderador definió. También está la opción de que los participantes creen otras categorías que tengan sentido para ellos.

**Ejemplo de Card Sorting online. Herramienta [OptimalSort](https://www.optimalworkshop.com/optimalsort/)**

Esta herramienta es muy fácil de usar y su funcionamiento se divide en tres partes.
1. La primera consiste en ingresar las *tarjetas*. Estas se encuentran en los elementos de contenido del sitio que te gustaría estudiar.
2. Luego configuramos las opciones y clasificamos las tarjetas. Esto te ayudará a segmentar los resultados por demografía y psicografía.
3. Como último paso tienes que iniciar el reclutamiento de los participantes. Este proceso se realiza por medio de un link que puedes enviar por correo a tus usuarios o clientes. Tambien puedes publicarlo en un sitio web o hasta tuitearlo.

![OptimalSort](img/optimalsort.png)

### Pruebas/encuestas con usuarios

Pasos a seguir:

**Paso 1: Preparar la prueba de usabilidad**:

- Crear un plan de prueba de usabilidad.
- Reclutar participantes para la prueba de usabilidad.
- Escribir guiones de prueba de usabilidad.
- Definir tareas de prueba de usabilidad.
- Sesión de prueba piloto de usabilidad.

**Paso 2: Realizar sesiones de prueba de usabilidad**:

- Preparar sesión.
- Realizar una sesión informativa con instrucciones previas a la sesión.
- Realizar una entrevista previa a la sesión.
- Sesión moderada.
- Realizar una entrevista posterior a la sesión.

**Paso 3: Comunicar resultados y hallazgos**:

- Analizar los hallazgos.
- Escribir informe de prueba de usabilidad.
- Vender hallazgos, es decir, convencer a las personas.

#### Plan de Prueba de usabilidad

El plan de Prueba de usabilidad debe ser breve y contener la información necesaria. Por lo general, una o dos páginas son suficientes.

Las partes interesadas y la gerencia revisan el plan y se realizan las modificaciones necesarias hasta que se considere aceptable.

Identificar los objetivos de la prueba de usabilidad. Estos podrían incluir descripciones muy cortas de las tareas claves para ser probado y los siguientes objetivos específicos:

- Evaluar si se han cumplido los requisitos de usabilidad.
- Descubrir problemas de usabilidad para que puedan corregirse.
- Demostrar de manera convincente a las partes interesadas que su producto de software contiene graves probiemas de usabilidad.

Identificar grupo de usuarios para la prueba de usabilidad:

- El Usuario realiza tareas determinadas.
- La evaluación debe ser guiada por un Facilitador.
- El Tomador de notas debe tomar notas de lo sucedido.
- El Observador es el interesado en el producto.

Cuestiones que hay que contemplar en este Plan de prueba de usabiidad:

- Técnica de pensar en voz alta.
- Número de participantes de la prueba planificada.
- Calendario y duración aproximada de cada sesión de prueba de usabilidad.
- Elegir el moderador responsable de la prueba de usabilidad.
- Una estimación de recursos y costos para la prueba de usabilidad, incluidas las horas de las personas y los incentivos.
- Lugar donde se realizará la prueba de usabilidad.
- Cómo se comunicarán les hallazgos.

##### Actores involucrados en la Prueba de usabilidad

**Moderador**. Persona neutral. Habla con el participante. Experto en Usabilidad. Guía la sesión de prueba.

**Participante**. Usuario representativo. Resuelve tareas que se orientan.

**Tomador de notas**. Experto en Usabilidad. Registra hallazgos importantes de usabilidad.

**Observador**. Persona interesada en el producto de software o
características requeridas. No es necesario que asista a la prueba.

##### Lugar de prueba

La ubicación de la prueba es el lugar donde se realiza la Prueba de usabilidad.

Ejemplos de ubicaciones de prueba:

- Un laboratorio de pruebas de usabilidad dedicado.
- Dos salas de oficina que están conectadas por un enlace de
vídeo.
- Una sala de oficina.
- Una habitación en el lugar donde vive o trabaja el participante de la prueba.
- Un lugar público, como una cafetería. Esta ubicación se elige con mayor frecuencia para sesiones de Prueba de usabilidad rápidas que duran diez minutos o menos.

##### ¿Cuántos usuarios son necesarios para realizar la Prueba de usabilidad?

[Nielsen Norman Group](https://www.nngroup.com/) recomienda 5 usuarios.

Después de 5 pruebas se tiende a encontrar los mismos patrones.

Esquema de Pruebas de Nielsen:

![Nielsen](img/nielsen.png)

¿Cuándo probar con más usuarios? Cuando un servicio/app tiene grupos de usuarios muy diferentes.

Planificar siempre con al menos 3 usuarios de cada categoría en cada grupo, para asegurarnos de haber cubierto la diversidad de comportamientos.

También se debe probar con más usuarios en estos casos:

- Estudios cuantitativos: probar al menos con 20 usuarios para obtener resultados estadísticamente significativos.
- Card sorting: probar al menos con 15 usuarios por cada grupo de usuarios.
- Eyetracking: probar con 39 usuarios si queremos mapas de calor estables.

#### Guión de Prueba de Usabilidad

Los scripts de Prueba de usabilidad son documentos que guían al facilitador y a los participantes a través de las sesiones de prueba, asegurando que los objetivos, tareas y preguntas sean consistentes y relevantes.

Contiene la siguiente información:

- Actividades para preparar la sesión de Prueba de usabilidad
antes de que llegue el participante a la prueba.
- Instrucciones informativas.
- Preguntas de la entrevista previa a la sesión.
- Tareas de la Prueba de usabilidad.
- Preguntas de la entrevista posterior a la sesión.

#### Tareas de la Prueba de usabilidad

Una buena tarea de prueba:

- Coincide con los objetivos de la prueba de usabilidad como se define en el plan de Prueba de usabilidad.
- Es relevante desde el punto de vista del participante de la prueba. Las tareas de prueba de usabilidad generalmente evitan solicitar tareas orientadas al sistema, como el inicio de sesión o el cambio de contraseña. En cambio, las buenas tareas son significativas para el participante de la prueba como, por ejemplo, comprar un producto.
- Es relevante desde el punto de vista de los interesados.

Una tarea de prueba de usabilidad contiene la siguiente información:

- La redacción precisa del escenario de la tarea de prueba de usabilidad que se entregará al participante de la prueba.
- Condiciones previas para la tarea, incluidos los recursos disponibles para el participante de la prueba.
- Una justificación de la importancia de la tarea, incluido lo que se pretende evaluar.
- Cualquier dato proporcionado al participante de la prueba para resolver la tarea, por ejemplo, una dirección de entrega, o información en la base de datos cuando el participante de prueba comienza la tarea.
- Criterios para completar la tarea o abandonar la tarea, incluido el resultado previsto o la respuesta esperada. Un criterio de muestra para el abandono de la tarea es *"si el participante de la prueba no ha encontrado una respuesta en 10 minutos, la tarea se abandona"*.

Puntos relevantes para la secuencia en la que se solicitan las tareas de prueba de usabilidad:

- Donde sea posible, la primera tarea debe ser simple para que los participantes de la prueba experimenten un éxito rápido. Esto es particularmente importante si un participante de la prueba parece
estresado por la situación.
- Probar tareas que son esenciales para el éxito del producto de software, antes de probar tareas de menor importancia.
- Las tareas se solicitan en un orden que parece lógico desde el punto de vista del participante de la prueba.
- Por ejemplo, se les indica a los participantes de la prueba que soliciten algo antes de recibir la tarea de cancelar un pedido.
- Si es posible, se evitan las tareas que dependen de la finalización exitosa de una tarea anterior.
- Cuando sea posible, la última tarea también debe ser simple para no permitir que el participante de la prueba concluya con una experiencia negativa y frustrante.

#### Actividades clave en una sesión de Prueba de usabilidad

**Preparación de la sesión**. El moderador prepara el hardware, el producto de software y las tareas de prueba para la sesión de prueba antes de que llegue el participante de la prueba.

**Instrucciones previas a la sesión informativa**. El moderador informa al participante de la prueba sobre el propósito de la prueba de usabilidad y cuál es su función y contribución.

**Entrevista previa a la sesión**. El participante de la prueba de usabilidad responde las preguntas del moderador sobre sus antecedentes y experiencia previa con el producto de software o
productos de software relacionados.

**Moderación**. El participante de la prueba resuelve las tareas de prueba de usabilidad que recibe del moderador. Al resolver tareas, se anima al participante de la Prueba de usabilidad a pensar en voz alta. El moderador observa en silencio al participante de la Prueba de usabilidad durante la solución de la tarea de prueba de usabilidad. El moderador guía al participante de la prueba si se atasca por completo, generalmente pasando a la siguiente tarea de prueba.

**Entrevista posterior a la sesión**. El participante de la prueba de usabilidad responde preguntas del moderador sobre su experiencia y la impresión general de la usabilidad del producto de software. Las preguntas clave tipo son: *"¿Qué 2-3 cosas te gustaron más del producto de software?"* y *"¿Qué 2-3 cosas necesitan mejorar?"*.

#### Ejemplo de Prueba de usabilidad con usuarios

Tarea:
Buscar vuelo.

Escenario:
Imagina que quieres ir con tu familia de vacaciones, pero quieres buscar un vuelo económico. Uno de tus amigos te sugiere usar Skyscanner. Busca un vuelo hacia el destino al que deseas viajar con tu familia.

![Ejemplo Prueba de usabilidad](img/ejemplo-prueba.png)

#### Análisis de los hallazgos

Se realizan los siguientes pasos:

1. Durante la sesión de Prueba de usabilidad, el tomador de notas registra las observaciones de usabilidad, generalmente escribiéndolas. Las observaciones de usabilidad reflejan eventos que causan problemas o tienen un efecto positivo en la efectividad, eficiencia y satisfacción.
2. Después de cada sesión de Prueba de usabilidad el tomador de notas y el moderador se reúnen para revisar las observaciones
tomadas de la sesión de Prueba de usabilidad.
3. Después de que se hayan completado todas las sesiones de Prueba
de usabilidad, el moderador y el tomador de notas extraen de sus
observaciones los principales hallazgos de usabilidad, tanto positivos como negativos. Estos hallazgos reflejan las observaciones que consideran más importantes.
4. El moderador y el tomador de notas se reúnen nuevamente y revisan sus hallazgos. Los resultados se fusionan en una lista común.
5. El moderador registra los problemas detectados en la herramienta de seguimiento de problemas de usabilidad de la empresa, que idealmente es lo mismo que la herramienta de seguimiento de defectos.
6. El moderador rastrea los problemas hasta su resolución y revisa la solución implementada. Si la solución implementada representa un riesgo deberá estar sujeta a otra Prueba de usabilidad.

#### Comunicación de Resultados y Hallazgos

Los resultados de una prueba de usabilidad son únicos en un aspecto:

Muestran lo que los usuarios representativos pueden lograr con el producto de software cuando realizan tareas representativas.

>Obtener las opiniones personales de los usuarios, o discutirlas, no respaldan este objetivo.

Ejemplos:

Tarea de Prueba de usabilidad: *"¿El diseño de la página de inicio es apropiado para la tienda de CD en línea?"*

Comentario: *"Puedo hacer esto fácilmente, pero la mayoría de los demás tendrán serios problemas"*. El comentario de un participante de la Prueba de usabilidad es una opnión personal. El moderador puede obtener información valiosa adicional al seguir este comentario con la pregunta: *"¿Por qué piensas eso?"*

Es aceptable informar los hallazgos que se basan en las opiniones de los participantes de la prueba de usabilidad sobre
un producto de software, por ejemplo:

*"El diseño de la página de inicio es realmente bonito"*.

Pero sólo si la mayoría de los participantes de la prueba de usabilidad lo expresan espontáneamente.

Un Hallazgo de usabilidad:

Es el resultado de una evaluación de usabilidad que identifica algún problema u oportunidad importante.

Los resultados positivos de usabilidad son importantes por las siguientes razones:

- Facilitan la venta de la necesidad de corregir los problemas de usabilidad al ofrecer una visión equilibrada.
- Comunican al equipo de desarrollo qué características no deben
modificarse o eliminarse.
- Permite obtener una vista completa de la usabilidad.

Un **informe de Prueba de usabilidad** debe contener un apartado que describa los hallazgos más importantes de la Prueba de usabilidad y las recomendaciones asociadas para mejorar el producto de software.

La descripción de cada hallazgo debe incluir los siguientes elementos:

- Clasificación de los hallazgos.
- Un encabezado que describe brevemente el hallazgo.
- Una descripción del hallazgo. Las declaraciones generales como *"Los mensajes de error no son útiles"* deben estar respaldadas con ejemplos.
- Citas relevantes de los participantes de la prueba relacionadas con el hallazgo (opcional)
- Recomendaciones para mejorar (opcional)
- Capturas de pantalla que ilustran el hallazgo (anexo opcional)
- Clasificación de gravedad de los resultados.

Las clasificaciones típicas son:

- Problema de usabilidad: cada problema de usabilidad debe tener una clasificación de gravedad, como se describe un poco más adelante.
- Hallazgo positivo: funciona bien. Este enfoque puede ser recomendado.
- Buena idea: una sugerencia de un participante de la prueba que podría conducir a una mejora significativa de la experiencia del usuario.
- Problema funcional: defecto.

Las clasificaciones de gravedad típicas de los problemas de usabilidad son:
- Menor: insatisfacción menor, retrasos notables o dificultades superficiales.
- Mayor: retrasos sustanciales, o insatisfacción moderada.
- Crítico: los participantes de la prueba se dieron por vencidos.
- Catastrófica: amenaza existencial. Potencialmente mortal, daño
corporativo o daño financiero sustancial.

Los parámetros importantes que influyen en las clasificaciones de gravedad son:

- Frecuencia: ¿con qué frecuencia ocurre el problema de usabilidad?
- Impacto: ¿en qué medida perjudica al usuario y al entorno del usuario cuando se produce el problema de usabilidad?
- Persistencia: ¿con qué rapidez aprenderán los usuarios a
evitar el problema de usabilidad?

#### Venta de resultados a las partes interesadas

El probador de usabilidad debe *vender* los hallazgos de usabilidad de manera convincente a todos los interesados y debe comprender su motivación y enfoque.

Si las partes interesadas no aceptan los resultados de una Prueba de usabilidad, el riesgo es que se producirán pocos cambios beneficiosos en la interfaz de usuario del producto de software.

Las partes interesadas deben participar en la planificación y ejecución de una prueba de usabilidad. Esto les permite
*comprar* la Prueba de usabilidad y hace que los problemas de usabilidad sean más fáciles de aceptar.

Las partes interesadas más importantes son las personas que deciden qué cambios de la Prueba de usabilidad deberían implementarse realmente, y las personas que realizan la implementación.

Formas de involucrar a los interesados:

- Participar en la evaluación de riesgos.
- Participar en la redacción y revisión del plan de Prueba de usabilidad, la Prueba de usabilidad, creación de guiones y, en particular, las tareas de la Prueba de usabilidad.
- Participar en el proceso de reclutamiento, especialmente en la definición del participante de la prueba y su perfil.
- Observar las Pruebas de usabilidad.

Hay que facilitar observar las sesiones de Pruebas de usabilidad:

- Programar sesiones de Prueba de usabilidad en horarios convenientes para las partes interesadas.
- Anunciar ampliamente las sesiones de Prueba de usabilidad e indicar que observar sólo una parte de la sesión de Prueba de usabilidad también es aceptable.
- Llevar a cabo sesiones de Prueba de usabilidad en lugares convenientes para las partes interesadas.
- Facilitar a las partes interesadas observar las sesiones de Prueba de usabilidad como grupo.
- Observar y debatir puede convencer a los interesados escépticos de los beneficios de las Pruebas de usabilidad.
- Participar en la revisión del informe de Prueba de usabilidad.

#### Informe de Prueba de usabilidad

Un informe de prueba de usabilidad contiene las siguientes secciones:
- Resumen ejecutivo.
- Tabla de contenido.
- Objetivos: descripción del objetivo de evaluación.
- Propósito: propósito de la evaluación, incluyendo listados o referencias a requisitos de usabilidad relevantes.
- Métodos de Evaluación.
- Descripción del enfoque utilizado.
- Información sobre el entorno físico y técnico en el que se realizó la Prueba de usabilidad.
- Resultados y recomendaciones.
- Anexos.

#### Consejos para Pruebas con usuarios

- Evaluar calidad de uso de la aplicación, **NO** al usuario.
- No explicar al usuario información detallada de la aplicación.
- Recordar que el usuario debe pensar en voz alta.
- Indicar tareas concretas con un lenguaje sencillo.
- Involucrar al equipo en el plan y resultados.

#### Encuesta de usuarios

Es una evaluación de usabilidad en la que se pide a una muestra representativa de usuarios que informe acerca de la evaluación subjetiva en un cuestionario, basado en su experiencia en el uso de un componente o sistema.

Las encuestas de usuarios se pueden utilizar para evaluar los niveles de satisfacción del usuario con un producto de software.

Pasos para realizar una Encuesta de usuarios:

1. Escribir un plan de encuesta.
2. Entrevistar a usuarios y partes interesadas.
3. Seleccionar un cuestionario.
4. Implementar el cuestionario.
5. Analizar las respuestas de la Encuesta de usuarios.
6. Comunicar los resultados de la Encuesta de usuarios.

Cuestionarios estandarizados de usabilidad:

- [SUMI](https://sumi.uxp.ie/en/index.php)
- [WAMMI](http://www.wammi.com/questionnaire.html)

## Experiencia de Usuario (UX)

![UX vs UI](img/ux-vs-ui.png)

**Usabilidad** es un atributo de calidad que mide lo fácil que son de usar las interfaces.

**Experiencia de Usuario** es lo que se siente cuando se interactúa con el producto de software.

**Interfaz de Usuario** es el medio con que el usuario puede comunicarse con el producto de software.

Ejemplo de una app:

 UI me permite comprar. Usabilidad me permite prevenir un error. UX es lo que percibo.

 > UX **NO** es agregar funcionalidad, es ponerse en los zapatos del usuario.

 La norma ISO 9241-210 define la experiencia de usuario (UX) como *"las percepciones y respuestas de las personas, resultantes del uso o anticipación de uso de un producto, sistema o servicio"*.

 La Experiencia de Usuario incluye las siguientes características del usuario que ocurren antes, durante y después del uso:

- Emociones.
- Creencias.
- Preferencias.
- Percepciones.
- Respuestas físicas y psicológicas.
- Comportamientos y logros.

La Experiencia de Usuario es influida por:

- Imagen de marca, es decir, la confianza de los usuarios en el fabricante.
- Presentación, es decir, la apariencia del producto de software, incluido el embalaje y la documentación.
- Funcionalidad.
- Rendimiento del producto de software.
- Comportamiento interactivo.
- La utilidad del producto de software, incluido el sistema de ayuda, soporte y capacitación.
- Capacidad de aprendizaje.
- El estado interno y físico del usuario resultante de experiencias previas, actitudes, habilidades, personalidad, educación e inteligencia.
- El contexto de uso.

Los criterios de usabilidad, como la efectividad, la eficiencia y la satisfacción, se pueden utilizar para evaluar aspectos de la experiencia del usuario como:

- La imagen y presentación de la marca (satisfacción).
- La funcionalidad (efectividad).
- El rendimiento del producto de software (eficiencia).

### Evaluación de la Experiencia de Usuario versus Usabilidad

La experiencia de usuario describe las percepciones y respuestas de una persona, que resultan del uso o uso anticipado de un producto de software.

La usabilidad es parte de la experiencia de usuario. En consecuencia, la evaluación de usabilidad es parte de la evaluación de la experiencia de usuario.

Las principales técnicas utilizadas para la evaluación de la experiencia de usuario son las mismas que las utilizadas para la
evaluación de la usabilidad.

Objetivos de la evaluación de la Experiencia de Usuario:

- Evaluar los servicios recibidos antes del uso del producto de software. Antes.
- Evaluar la interacción directa entre los usuarios y el producto de software. Durante.
- Evaluar los servicios recibidos después del uso del producto de software. Después.

La evaluación de la experiencia de usuario aborda toda la experiencia del usuario con el producto de software, no sólo la interacción directa.

La Experiencia de Usuario incluye:

- Anuncios que hacen que los usuarios conozcan el producto de software.
- Capacitación en el uso del producto de software.
- Puntos de contacto con el producto de software que no sean diálogos en pantalla, como encuentros con soporte, cartas o productos recibidos como resultado de la interacción con el producto de software.
- Problemas que no son manejados por la interfaz de usuario del producto de software, como notificaciones de demoras, manejo de quejas y llamadas no solicitadas.

Riesgos en la Experiencia de Usuario:

Riesgos típicos:
- Los usuarios no pueden comprar un producto porque en la aplicación se modificó el flujo de compra: no realiza algunas de las funciones requeridas por los usuarios (falta de efectividad), realiza las funciones previstas lenta o torpemente, es desagradable de usar o no proporciona satisfacción.
- El producto de software es utilizable, pero los artefactos y
procedimientos asociados no: el soporte responde a las consultas razonables de los clientes de forma lenta, ruda, superficial, o ninguna, la documentación del usuario es deficiente, no se comunican demoras en la entrega de los productos pedidos, el producto entregado no coincide con las expectativas de los usuarios, el producto llega en un embalaje poco atractivo, o poco práctico.

Técnicas de análisis y definición de UX

![Técnicas de análisis y definición de UX](img/tecnicas.png)

- **Técnicas con usuarios**: es la técnica estrella en el diseño centrado en el usuario, pues ofrece los resultados más completos y fiables en lo que a usabilidad UX de un diseño se refiere. Consiste en observar cómo llevan a cabo ciertas tareas indicadas por el Facilitador un grupo de usuarios, lo cual permitirá detectar y analizar los problemas de usabilidad que vayan
surgiendo.
- **Evaluación heurística**: no requiere de la participación de los usuarios, sino que es una técnica de inspección, en la que varios expertos examinan el diseño en busca de problemas de usabilidad y en base a las heurísticas de usabilidad que definen un diseño usable.
- **Card Sorting**: solicitar al grupo de usuarios participantes que asocien determinados conceptos representados en tarjetas, en función de su similitud semántica.
- **Eye Tracking**: conocido también como seguimiento visual, se basa en tecnologías que permiten monitorizar la manera en que el usuario mira una imagen.
- **Etnografía**: se basa en el estudio y descripción científica del comportamiento, conducta, acciones de los usuarios que son objeto de la investigación.

¿Por qué la Experiencia de Usuario es importante en
cada proyecto?:

- Aumenta las conversiones.
- Reduce significativamente los costos de desarrollo en el futuro.
- Aumenta la fidelidad de tus clientes hacia tu marca.
- Aumenta las referencias de tus usuarios.

## Pruebas de accesibilidad

**Curb Cut Effect**

![Curb Cut Effect](img/curb-cut-effect.png)

La falta de accesibilidad nos impacta a TODOS.

Las mejoras de accesibilidad nos favorecen a TODOS.

>"When we design for disability first, you often stumble upon solutions that are better than those when we design for the norm."
>
>Elise Roy @TED talk. Lawyer, artist, human rights advocate

Donde se penso que habia un beneficio para algunos, hoy en día el beneficio es para todos, estemos o no en situación de discapacidad.

### Productos de apoyo

![Apoyo Visual](img/apoyo-visual.png)

![Apoyo auditivo](img/apoyo-auditivo.png)

![Apoyo Motriz](img/apoyo-motriz.png)

![Apoyo Cognitivo](img/apoyo-cognitivo.png)

### [WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/glance/)

![Historia WCAG](img/historia-wcag.png)

![WCAG 2.1](img/wcag-2-1.png)

### Principios de accesibilidad

![Principios de Accesibilidad](img/principios-accesibilidad.png)

#### Principio Perceptible

Se centra en la información y los componentes de la interfaz de usuario.

*"¿Hay algo en nuestra aplicación que una persona con baja visión, o algún tipo de discapacidad visual no pueda percibir?"*

Consejos: 

- Proporcionar alternativas textuales para contenido no textual.
- Proporcionar subtítulos y otras alternativas para multimedia.
- Crear contenido que se pueda presentar de diferentes formas, incluyendo a las tecnologías de apoyo, sin perder información.
- Facilitar que los usuarios puedan ver y oír el contenido.

**Textos alternativos**

![Textos alternativos](img/textos-alternativos.png)

![Textos alternativos](img/textos-alternativos-2.png)

![Textos alternativos](img/textos-alternativos-3.png)

![Textos alternativos](img/textos-alternativos-4.png)

![Textos alternativos](img/textos-alternativos-5.png)

**Zoom**

![Zoom](img/zoom.png)

**Contraste de colores**

![Contraste de colores](img/contraste-colores.png)

#### Principio Operable

Se centra en las acciones sobre los componentes de la interfaz de usuario y la navegación.

*"¿Pueden los usuarios controlar los elementos interactivos de nuestro aplicación?"*

*"¿Nuestra aplicación funciona bien usando sólo el teclado?"*

Consejos:

- Proporcionar acceso a todas las funcionalidades mediante
el teclado.
- Conceder a los usuarios tiempo suficiente para leer y usar
el contenido.
- No usar contenido que pudiera causar convulsiones o reacciones físicas.
- Ayudar a los usuarios a navegar y encontrar el contenido.
- Facilitar métodos de entrada diferentes al teclado.

**Ubicación**

![Ubicación](img/ubicacion.png)

**Foco**

![Foco](img/foco.png)

Problemas más frecuentes con el Foco:

**El foco no está visible**: 

- El foco debe estar visible.
- No usar `:focus { outline:0; }`.
- Puedes usar `:focus` y `:hover` para resaltar más claramente que
un elemento tiene el foco.

**El orden del foco sin sentido**:

- Orden del contenido correcto en el código HTML
- Si se cambia el orden del foco (por ejemplo con `Tabindex`) debe seguir teniendo sentido.

**Trampas para el foco**:

- Zonas de la página a las que podemos llegar mediante el teclado, pero de las cuales sólo podemos salir, para regresar a otra parte de la página, mediante el raton.

**Recibir el foco provoca un cambio de contexto**:

- Los cambios de contexto sólo deberían producirse cuando el usuario realiza una acción que habitualmente se utilice para solicitar un cambio de contexto, como hacer clic en un enlace o botón. No redireccionar a otra página (interna o externa) cuando un elemento recibe el foco.

**TAB Order**:

- TAB - movemos el foco adelante.
- SHIFT+TAB - movemos el foco hacia atrás.
- Flechas del teclado - movemos el foco dentro de un componente.
- OPTION + TAB - cambia el foco en otros navegadores, como Safari.
- Espacio - seleccionar un `checkbox`.
- Flecha derecha del teclado - seleccionar un `radio button`.

#### Principio Comprensible

Se centra en que la información sea entendible y en la interacción con la interfaz de la aplicación.

*"¿Está todo el contenido claramente escrito?"*

*"¿Todas las interacciones son fáciles de entender?"*

Consejos:

- Proporcionar texto legible y comprensible.
- Proporcione contenido que sea predecible en apariencia y operación.
- Ayudar a los usuarios a evitar y corregir errores.

**Texto legible**

![Texto legible](img/texto-legible.png)

#### Principio Robusto

Se centra en que el contenido de nuestras aplicaciones sea interpretado de forma fiable por los productos de apoyo.

*"¿Nuestra aplicación sólo es compatible con los navegadores o sistemas operativos más recientes?"*

*"¿Funciona correctamente con el lector de pantalla?"*

Consejos:

- Maximizar la compatibilidad con herramientas de usuario actuales y futuras.

### Herramientas de Evaluación Automática y Técnicas de Filtrado

**Herramientas para chequear la accesibilidad**:

- [Colour Contrast Analyzer - CCA](https://www.tpgi.com/color-contrast-checker/).
- [WAVE](https://wave.webaim.org/).
- [axe](https://www.deque.com/get-started-axe-devtools-browser-extension/).
- [Total Validator](https://www.totalvalidator.com/): de pago.
- [Pa11y](https://pa11y.org/).

Hay una lista de software disponible en https://www.w3.org/WAI/test-evaluate/tools/list/

**El testing automatizado no puede sustituir a las pruebas manuales y el feedback real de los usuarios.**

**Técnicas de filtrado**:

- Revisión del contraste de colores con una app.
- Zoom.
- Navegar en la aplicación sólo con el uso del teclado.
- Prueba con lectores de pantalla.
- Comprobación del foco.
- Orden de tabulación.

## Diseño accesible

Los errores más comunes que detectamos al probar aplicaciones los encontramos en el contraste entre los colores.

### [Colour Contrast Analyzer](https://www.tpgi.com/color-contrast-checker/) 

Es una app, que se instala en el ordenador, que nos permite validar el contraste entre el color del fondo y el del primer plano.

**Contrast Ratio (Contraste de colores)**

Es la relación de la luminiscencia relativa de los colores de primer plano y de fondo.

Varía desde un mínimo de 1:1 (cuando los colores de primer plano y de fondo son los mismos), hasta un máximo de 21:1 (negro sobre blanco).

Para prevenir estos errores y conocer si cumplimos o no con los requisitos de contraste, la WCAG 2.1 nos plantea:

![Contrast Ratio](img/contrast-ratio.png)

No tienen un requisito mínimo de contraste:

- Textos grandes. Textos e imágenes grandes que tengan una relación de contraste mínimo de 3:1.
- Textos o imágenes que son parte de la decoración y no son
contenidos significativos con los cuales el usuario interactúa.
- Logotipos que contengan texto o el nombre de una marca.

### [Contrast Finder](https://app.contrast-finder.org/)

 Es una herramienta online que evalúa el contraste entre dos colores (fondo, primer plano) y comprueba si el contraste es aceptable. Cuando el contraste no es aceptable, el objetivo principal es sugerir alternativas de contraste de color válidas.

Está diseñada para ser utilizada por diseñadores web, desarrolladores web, o profesionales de la accesibilidad web para mejorar la facilidad de lectura de páginas HTML en sitios web.

### Validadores de comprobación automática

**aXe**: se puede instalar como extensión de Chrome.

**WAVE**: se puede evaluar online en su misma web. Tiene extensiones para Chrome y Firefox.

**Test Accesibilidad (Android)**: es una app para móvil que se puede descargar en la Play Store.

**Pa11y**: es una herramienta que funciona desde consola. https://github.com/pa11y/pa11y

````bash
pa11y [options] <url>
````
El campo **options** permite personalizar el tipo de testeo que se desea realizar, los principales son:

- -s, --standard <name>. Se elige el estándar de accesibilidad con el cual se va a realizar el testeo. Por defecto se toma WCAG2AA.
- -r, --reporter < reporter>. Hace referencia a cómo se devuelven los resultados obtenidos. Si no se incluye esta opción, los resultados se muestran por consola. Las opciones de formato
disponibles incluyen: cli (por defecto), csv, tsv y json.
- -I, -level <level>. El nivel de mensaje con el cual falla (error, aviso, advertencia).
- -e, --runner <runner>: Los motores de prueba para usar: htmlcs (predeterminado), aXe.
-  --include-notices. Incluir avisos en el informe.
- --include-warnings. Incluir advertencias en el informe.

Ejecutar una prueba de accesibilidad contra una URL:

````bash
pally https://example.com
````

Ejecutar una prueba de accesibilidad contra un archivo local (sólo rutas absolutas, no relativas):

````bash
pally ./path/to/your/file.html
````

Ejecutar una prueba con informe CSV y guardarla en un archivo:

````bash
pally --reporter csv https://example.com › report.csv
````

Ejecutar Pa11y usando aXe para realizar la prueba:

````bash
pally --runner axe https://example.com
````

